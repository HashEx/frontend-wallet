import React, { useEffect, useState, useCallback } from 'react';
import { Button, Input, Form } from 'antd';

import * as walletService from '../services/walletService';

import Transactions from './Transactions';

const bip39 = require('bip39');

const useJSBGL = () => {
    let [jsbgl, setJSBGL] = useState(null);
    if(typeof window.jsbtc !== 'undefined'){
        const jsbtc = window.jsbtc;
        jsbtc.asyncInit().then(() => {
            setJSBGL(jsbtc);
        })
    }
    return jsbgl;
};

const useWallet = () => {
    const jsbgl = useJSBGL();
    const [privateKey, setPrivateKey] = useState(null);
    const [seed, setSeed] = useState(null);
    const [wallet, setWallet] = useState(null);
    const create = useCallback((pk) => {
        if(jsbgl){
            const data = new jsbgl.Address(pk);
            walletService.importAddressToNode(data.address)
            setPrivateKey(pk);
            setWallet(data);
        }
    }, [jsbgl]);
    const createWithSeed = useCallback((mnemonic) => {
        if(jsbgl){
            const seed = jsbgl.mnemonicToSeed(mnemonic);
            const xPriv = jsbgl.createMasterXPrivateKey(seed);
            const derived = jsbgl.deriveXKey(xPriv, "m/44'/0'/0'");
            const wif = jsbgl.privateFromXPrivateKey(derived, {wif: true});
            const data = new jsbgl.Address(wif);
            walletService.importAddressToNode(data.address)
            setPrivateKey(wif);
            setSeed(mnemonic);
            setWallet(data);
        }
    }, [jsbgl])
    const clear = () => {
        setWallet(null);
        setPrivateKey(null);
    }
    return {
        privateKey,
        wallet,
        create,
        createWithSeed,
        clear,
        jsbgl,
        seed,
    }
}

const SendForm = ({onSend, maxAmount}) => {
    const [address, setAddress] = useState("bgl1quzj5y3gz44cpc70d0j6n7l82h60a8v0rwf0pjq");
    const [amount, setAmount] = useState(maxAmount);
    const [loading, setLoading] = useState(false);
    const clear = () => {
        setAddress(null);
        setAmount(null);
    }
    const onChange = (name) => (event) => {
        if(name === "address") setAddress(event.target.value);
        if(name === "amount") setAmount(event.target.value);
    }
    const handleClick = async () => {
        setLoading(true);
        await onSend(address, amount)
        setLoading(false);
        clear()
    }
    const disabled = !address || !amount;
    return (
        <div class="send-form">
            <Form layout="vertical">
                <Form.Item label="Address">
                    <Input value={address} onChange={onChange("address")} disabled={loading} />
                </Form.Item>
                <Form.Item label="Amount">
                    <Input value={amount} onChange={onChange("amount")} disabled={loading} />
                </Form.Item>
            </Form>
            
            <Button onClick={handleClick} disabled={disabled} loading={loading}>Send</Button>
        </div>
    )
}

const Balance = ({wallet, onSend}) => {
    const [balance, setBalance] = useState(null);
    useEffect(() => {
        if(wallet){
            walletService.getBalance(wallet).then((response) => {
                setBalance(response)
            })
            .catch(e => {
                console.error(e);
            })
        }
    }, [wallet])
    return (
        <div class="balance">
            <div class="balance__title">Balance: {balance} BGL</div>
            <SendForm onSend={onSend} maxAmount={balance} />
        </div>
    )
}

const Wallet = () => {
    const [pk, setPK] = useState(null);
    const [seed, setSEED] = useState(bip39.generateMnemonic());
    const {wallet, create, createWithSeed, clear, privateKey, jsbgl} = useWallet();

    const COMISSION = 300;
    const MULTIPLIER = Math.pow(10, 8);
    
    const onSend = async (sendAddress, sendAmount) => {
        let tx = new jsbgl.Transaction({lockTime: 0});

        sendAmount = Math.round(sendAmount * MULTIPLIER * 1000) / 1000;
        
        const unspents = await walletService.getUnspent(wallet.address)
        let totalUnspents = 0
        unspents.forEach((unspent) => {
            const value = unspent.amount * MULTIPLIER;
            totalUnspents += value;
            tx.addInput({
                txId: unspent.txid,
                vOut: unspent.vout,
                address: unspent.address
            });
        });

        try {

            if(sendAmount > totalUnspents + COMISSION) throw new Error("Not enough funds")

            tx.addOutput({value: sendAmount, address: sendAddress});
            
            totalUnspents = totalUnspents - sendAmount - COMISSION; // 300 - comission

            tx.addOutput({value: totalUnspents, address: wallet.address});
        } catch(e) {
            console.error(e);
        }

        unspents.forEach((unspent, index) => {
            const value = unspent.amount * MULTIPLIER;
            tx.signInput(index, {
                privateKey,
                sigHashType: jsbgl.SIGHASH_ALL,
                value,
            });
        })
        
        try {
            
            const signedTx = tx.serialize();
            await walletService.sendTransaction(signedTx);
            alert('Success');
        } catch(e) {
            console.error(e);
            alert('Error: ', e.message);
        }
    }
    const onOpen = () => create(pk);
    const onOpenWithSeed = () => createWithSeed(seed);

    const onChangeSeed = (event) => {
        setSEED(event.target.value);
    };

    const onChangePK = (event) => {
        setPK(event.target.value);
    };
   
    return wallet ? (
        <div>
            <Button onClick={clear}>
                Reset
            </Button>
            <div>
                Wallet: {wallet.address}
            </div>
            {wallet && <Balance wallet={wallet.address} onSend={onSend} />}
            <Transactions wallet={wallet.address} />
        </div>
    ) : (
        <div className="enter-form">
            <div class="enter-form__title">Bitgesell Wallet</div>
            <Form layout="vertical">
                <Form.Item label="Enter with seed">
                    <Input.TextArea rows={3} value={seed} onChange={onChangeSeed} />
                    <Button onClick={onOpenWithSeed} disabled={!seed}>
                        Open
                    </Button>
                </Form.Item>
                <div>or</div>
                <Form.Item label="Enter with private key">
                    <Input value={pk} onChange={onChangePK} />
                    <Button onClick={onOpen} disabled={!pk}>
                        Open
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}

export default Wallet;
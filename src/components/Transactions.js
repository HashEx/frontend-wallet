import React, { useState, useEffect } from 'react';
import { getTransactions } from '../services/walletService';

import Loader from './Loader';
import Transaction from './Transaction';

const Transactions = ({wallet}) => {
    const [loading, setLoading] = useState(false);
    const [txs, setTxs] = useState([]);
    useEffect(() => {
        setLoading(true);
        getTransactions(wallet).then((response) => {
            setTxs(response)
            setLoading(false);
        })
        .catch(e => {
            setLoading(false);
        }) 
    }, [wallet])
    return (
        <div class="transactions">
            <div class="transaction__title">Transactions:</div>
            {loading && <Loader />}
            <table class="transactions__table">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Fee</th>
                        <th>Block number</th>
                        <th>ID</th>
                    </tr>
                </thead>
                <tbody>
                    {txs.map((tx) => <Transaction {...tx} />)}
                </tbody>
            </table>
        </div>
    )
}

export default Transactions;
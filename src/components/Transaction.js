import React from 'react';

/*
    amount: number;
    fee?: number;
    blockheight: number;
    txid: string;
    category: string;
    confirmations: number;
    label: string;
    time: number;
*/

const Transaction = ({amount, time, fee, txid, blockheight}) => {
    const type = amount > 0 ? "in" : "out";
    return (
        <tr class="tx">
            <td>
                <div class="tx__time">{new Date(time * 1000).toLocaleDateString()}</div>
            </td>
            <td>
                <div class="tx__type">{type}</div>
            </td>
            <td>
                <div class="tx__amount">{Math.abs(amount)} BGL</div>
            </td>
            <td>
                {fee ? <div class="tx_fee">{Math.abs(fee)} BGL</div> : null}
            </td>
            <td>
                <div class="tx__block">{blockheight}</div>
            </td>
            <td>
                <div class="tx__id">
                    {txid}
                </div>
            </td>
        </tr>
    )
}

export default Transaction;
import axios from 'axios';

const api = axios.create({
    baseURL: 'https://bgl.hashex.org'
});

export const getTransactions = async (wallet, page = 1, limit = 10) => {
    if(page > 1) return Promise.resolve([]);
    const response = await api.get(`/listtransactions/${wallet}`, {params: {page, limit}});
    return response.data;
    
};

export const getBalance = async (wallet) => {
    const response = await api.get(`/getbalance/${wallet}`);
    return response.data;
};

export const importAddressToNode = async (wallet) => {
    const response = await api.get(`/importaddress/${wallet}`);
    return response.data;
}

export const getUnspent = async (wallet) => {
    const response = await api.get(`/listunspent/${wallet}`);
    return response.data;
}

export const sendTransaction = async (tx) => {
    const data = {
        "jsonrpc": "1.0",
        "id": "curltext",
        "method": "sendrawtransaction",
        "params": [tx]
    }
    const response = await axios.post('https://bgl-node.hashex.org/', data, {
        auth: {
            username: 'rpc',
            password: '123321'
        }
    });
    return response.data;
}

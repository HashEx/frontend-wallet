import 'antd/dist/antd.css';

import Wallet from './components/Wallet';

import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Wallet />
      </header>
    </div>
  );
}

export default App;

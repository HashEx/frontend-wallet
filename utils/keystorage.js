import { AsyncStorage } from 'react-native';
const CryptoJS = require('crypto-js');
const bip39 = require('bip39');

const Config = require('./config');

export const privateKeyToAccount = (key) => {
    return key;
}

let account = null;

export const getAccount = () => {
    if(!Config.USE_HARDCODED_PK) {
        if (!global.account) {
            throw new Error('Key is not decoded. Please try to logout and login again.');
        }
        return global.account;
    } else {
        if(!account) {
            account = privateKeyToAccount(Config.HARDCODED_PK);
        }
        return account;
    }
}

const encodeKey = (pin, key) => {
    const encoded =  CryptoJS.AES.encrypt(key, pin).toString();
    return encoded;
}

const decodeKey = (pin, encodedKe) => {
    const bytes  = CryptoJS.AES.decrypt(encodedKey, pin);
    const key = bytes.toString(CryptoJS.enc.Utf8);
    return key;
}

const privateKeyFromMnemonic = (mnemonic) => {
    const seed = bip39.mnemonicToSeedSync(mnemonic).toString('hex');
    const HDKey = require('hdkey');
    const hdkey = HDKey.fromMasterSeed(new Buffer(seed, 'hex'));
    const ethKey = hdkey.derive("m/44'/60'/0'/0/0");
    const privateKey = ethKey.privateKey;
    return privateKey.toString('hex');
}

export const unlockKey = async (pin) => {
    const encoded = await localStorage.getItem('privateKey');
    const unlockedKey = decodeKey(pin, encoded);
    global.account = privateKeyToAccount(unlockedKey);
    return unlockedKey;
}

export const checkMnemonic = async (mnemonic) => {
    const pin = (await localStorage.getItem('pin') || '');
    const privateKey = await unlockKey(pin);
    const isCorrectMnemonic = privateKey === privateKeyFromMnemonic(mnemonic);
    if(!isCorrectMnemonic){
        throw new Error('Entered words are not correct');
    }
    return;
}

export const storeKey = async (pin, mnemonic) => {
    const unlockedKey = privateKeyFromMnemonic(mnemonic);
    global.account = privateKeyToAccount(unlockedKey);
    await localStorage.setItem('pin', pin);
    await localStorage.setItem('privateKey', encodeKey(pin, unlockedKey));
    return global.account;
}

export const clear = async () => {
    await localStorage.removeItem('pin');
    await localStorage.removeItem('privateKey');
    return true;
}
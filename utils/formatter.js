export const numberWithCommas = (x) => {
    // console.log('formatting', x)
    // return x;
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
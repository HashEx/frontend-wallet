export const isNumber = (value) => {
    return !isNaN(value);
}

export const isStrBlank = (str) => {
    return str.trim() == '';
}

export const onlyLetters = (str) => {
    const regex = new RegExp(/^[A-Za-z]+$/);
    return regex.test(str);
}